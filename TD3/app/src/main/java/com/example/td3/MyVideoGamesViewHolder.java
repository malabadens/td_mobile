package com.example.td3;

import android.view.View;
import androidx.annotation.NonNull;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder  {

    private TextView mNameTV;
    private TextView mPriceTV;

    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);

        mNameTV = itemView.findViewById(R.id.Name);
        mPriceTV = itemView.findViewById(R.id.Price);

    }

    void display(JeuVideo jeuVideo){
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
    }
}
