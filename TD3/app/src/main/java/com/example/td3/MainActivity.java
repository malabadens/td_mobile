package com.example.td3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JeuVideo PUBG = new JeuVideo("PUBG",40);
        JeuVideo Fifa = new JeuVideo("FIFA 20",70);
        JeuVideo Battlefield = new JeuVideo("Battlefield 3",120);

        ArrayList mesJeux = new ArrayList<JeuVideo>();
        mesJeux.add(PUBG);
        mesJeux.add(Fifa);
        mesJeux.add(Battlefield);

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter( new MyVideosGamesAdapter(mesJeux));
    }
}
