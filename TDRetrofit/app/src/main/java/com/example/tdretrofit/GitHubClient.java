package com.example.tdretrofit;

import com.example.tdretrofit.RepoList;

import java.util.List;
import retrofit2.*;
import retrofit2.http.GET;
import retrofit2.http.Path;



public interface GitHubClient {

    @GET("users/{username}/repos")
    Call<List<RepoList>> UserRepositories(@Path("username") String userName);


}
